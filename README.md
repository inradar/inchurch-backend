# inChurch Developer Recruitment  #

This stage of the recruitment process consists on the development of a small API to manage users in a company. The challenge should be coded using Python with Django.

** Required Endpoints **

* Endpoint for user's registration - To register a user you need to send an email, a password, a full name and the department he/she works
* Endpoint for user's authentication - To authenticate a user you need to send it's username and password.
* Endpoint to read the user profile list - Show only full name, and profile identifier
* Endpoint to read the user profile detail - Show all info
* Endpoint to update a user profile (email, full name and department)
* Endpoint to change the user's password - To change a password you need to send the old password and the new password
* Endpoint to delete a user profile
* Endpoint to create a department - To create a department you only need to send it's name
* Endpoint to delete a department

** Required Behaviour **

* A user that is not authenticated is only allowed to register or login
* An non admin user can only update users from the same department he/she is in
* An non admin user can only delete his/her own profile
* A department can only be created or deleted by an admin user
* An error message should be returned any time an error occur
* User profile should be returned after it's registration
* Use the authorization method of your choice and describe it on the docs

### What should I do? ###

* Fork this repository (Click on the '+' button on the left menu and then on 'Fork this repository')
* Develop the aplication using best practices (write your code and comments in english)
* Create unit test cases for the whole application
* Document the API using the tool of your choice
* Overwrite this README file and describe how to set up your project and how to run tests
* Create a pull request when you are done

### What is going to be evaluated? ###

* Code quality
* Test and documentation coverage
* Git usage
* Development time
* Surprising us ;)

### Do you have any questions? ###

Contact: rafael.reis@inchurch.com.br